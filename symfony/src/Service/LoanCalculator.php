<?php

namespace App\Service;

use Exception;

class LoanCalculator
{
	
	/**
	 * @param  int  $principal
	 * @param  float  $interestRate
	 * @param  int  $term
	 *
	 * @return float
	 * @throws Exception
	 */
	public function getMonthlyPayment(int $principal, float $interestRate, int $term): float
	{
		//	Check for valid term
		if ($term < 0) {
			throw new Exception('invalid term');
		}
		
		if ($interestRate < 0) {
			throw new Exception('invalid Interest rate');
		}
		
		if ($principal < 0) {
			throw new Exception('invalid principal amount');
		}
		
		//  special case when full principal needs would be due immediately (prevents division by zero)
		if ($term == 0) {
			return $principal;
		}
		
		if ($interestRate > 0) {
			//	Get equivalent monthly interest rate as a multiplier
			$monthlyInterestRate = $interestRate / (12 * 100);
			
			$monthly_payment = $principal * ($monthlyInterestRate / (1 - pow((1 + $monthlyInterestRate), -$term)));
		} else {
			$monthly_payment = $principal / $term;
		}
		
		return round($monthly_payment, 2);
	}
	
	public function getApr(int $principal, float $interestRate, int $term, int $fees): float
	{
		//	Check for valid term
		if ($term < 0) {
			throw new Exception('invalid term');
		}
		if ($interestRate < 0) {
			throw new Exception('invalid Interest rate');
		}
		if ($principal < 0) {
			throw new Exception('invalid principal amount');
		}
		
		$interestPaid = $principal * $interestRate * ($term / 12);
		$apr = ($interestPaid + $fees) / $principal / ($term * 30) * 365;
	
		return number_format((float)$apr, 2, '.', '');
	}

	public function getNextAmortizationTableRow($month, $monthlyPayment, $monthlyInterestRate, $unpaidBalance)
	{
		// Using the payment, interest, and the previous row's data, calculate the values for the next row

		$interest = $monthlyInterestRate * $unpaidBalance;
		$unpaidBalanceReduction = $monthlyPayment - $interest;
		$unpaidBalance = $unpaidBalance - $unpaidBalanceReduction;

		if ($unpaidBalance < 0) {
			$unpaidBalanceReduction += $unpaidBalance;
			$unpaidBalance = 0;
		}

		$nextRow = array(
			$month,
			$monthlyPayment,
			number_format((float)$interest, 2, '.', ''),
			number_format((float)$unpaidBalanceReduction, 2, '.', ''),
			number_format((float)$unpaidBalance, 2, '.', ''));

		return $nextRow;
	}
}

