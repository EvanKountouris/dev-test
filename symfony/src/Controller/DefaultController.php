<?php

namespace App\Controller;

use App\Form\LoanType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\LoanParameter;
use App\Service\LoanCalculator;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{
	
	/**
	 * @Route("/", name="home")
	 */
	public function home(Request $request, LoanParameter $loanParameterService, LoanCalculator $loanCalculatorService): Response
	{
		$form = $this->createForm(LoanType::class);
		
		$formErrors = [];
		$loanData = [];
		$amortizationTable = [];
		
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			
			//  Process submitted data
			try {
				//  Check for requesting too large of an amount
				if ($data['amount'] > $loanParameterService->getMaxAmount($data['creditScore'])) {
					$formErrors[] = 'The maximum loan amount for your credit score is: $'.$loanParameterService->getMaxAmount($data['creditScore']);
				}
			} catch (\Exception $e) {
				$formErrors = ['Please check your inputs and resubmit the form'];
			}
			
			
			if (empty($formErrors)) {
				try {
					// Calculate the loan info
					$interestRate = $loanParameterService->getInterestRate($data['term'], $data['creditScore']);
					$fee = $loanParameterService->getOriginationFee($data['amount']);
					$payment = $loanCalculatorService->getMonthlyPayment($data['amount'] + $fee, $interestRate, $data['term']);
					$apr = $loanCalculatorService->getApr($data['amount'], $interestRate, $data['term'], $fee);
					
					$loanData['interestRate'] = $interestRate;
					$loanData['fee'] = $fee;
					$loanData['payment'] = $payment;
					$loanData['apr'] = $apr;

					// Check that the loan payment is not more than 15% of their monthly income
					if ($data['monthlyGrossIncome'] * 0.15 < $loanData['payment']) {
						$formErrors[] = 'The loan payment ($'.$loanData['payment'].') is more than 15% of your monthly income';
					}
				} catch (\Exception $e) {
					$formErrors = ['Please check your inputs and resubmit the form'];
				}
			}


			if (empty($formErrors)) {
				try {
					// create amortization table

					$monthlyInterestRate = $interestRate / 1200;
					$unpaidBalance = $data['amount'];
					$term = $data['term'];


					$amortizationTable = array (
						array(0, '', '', '', "$".$unpaidBalance)
					);

					for ($k = 1; $k <= $term; $k++) {
						$nextRow = $loanCalculatorService->getNextAmortizationTableRow($k, $payment, $monthlyInterestRate, $unpaidBalance);
						$unpaidBalance = $nextRow[4];
						array_push($amortizationTable, $nextRow);
					}

				} catch (\Exception $e) {
					$formErrors = ['Failure creating amortization table'];
				}
			}
			

			if (empty($formErrors)) {
				// Create and send pdf (if applicable)
				// Need Mpdf and phpmailer
			
				$pdf = new \Mpdf\Mpdf();

				$text = '';
				$text .= '<h1>Loan Tool Summary<\h1>' . '<br />';
				$text .= '<strong>Loan Amount: $</strong>' . $data['amount'] . '<br />';
				$text .= '<strong>Loan Term: </strong>'. $data['term'] . '<br />';
				$text .= '<strong>Credit: </strong>'. $data['creditScore'] . '<br />';
				$text .= '<strong>Origination Fee: $</strong>'. $fee . '<br />';
				$text .= '<strong>Interest Rate: %</strong>'. $interestRate . '<br />';
				$text .= '<strong>Monthly Payment: $</strong>'. $payment . '<br />';
				$text .= '<strong>APR: %</strong>'. $apr . '<br />';

				$pdf->WriteHTML($text);

				$attachment = $pdf->Output("","S");


				// "sendEmail" from PHPMailer doc
				if ($data['emailAddress'] != '') {

					$mail = new PHPMailer(true);

					try {
						//Server settings (cannot be done on symfony local server, so could not be tested)
						$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    					$mail->isSMTP();                                            //Send using SMTP
    					$mail->Host       = 'smtp.example.com';                     //Set the SMTP server to send through
    					$mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    					$mail->Username   = 'user@example.com';                     //SMTP username
    					$mail->Password   = 'secret';                               //SMTP password
    					$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    					$mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    					//Recipients
    					$mail->setFrom('from@example.com', 'Mailer');
    					$mail->addAddress($data['emailAddress']);               //Name is optional

    					//Attachments
						$mail->addStringAttachment($attachment, "LoanInfo.pdf");

    					//Content
    					$mail->isHTML(true);                                  //Set email format to HTML
    					$mail->Subject = 'Your Loan Information';
    					$mail->Body    = 'Here is <b>Your Loan Information</b>';

    					$mail->send();
    					echo 'Message has been sent';
					} catch (Exception $e) {
    					echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
					}
				}
				// End sendEmail


			}

		}
		
		return $this->render(
			'index.html.twig',
			[
				'form'       		=> $form->createView(),
				'formErrors' 		=> $formErrors,
				'loanData'   		=> $loanData,
				'amortizationTable' => $amortizationTable
			]
		);
	}
	
	
}
